//
//  ViewController.swift
//  PeopleListForScorp
//
//  Created by Umut ÖZBEK on 19.08.2021.
//

import UIKit

class ViewController: UIViewController {
    
    // UI Components
    private lazy var refreshController: UIRefreshControl = {
        var refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        return refreshControl
    }()
    private lazy var endOfListLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        label.textAlignment = .center
        label.text = "I'm tired, can not generate more."
        return label
    }()
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyLabel: UILabel!
    
    // Variables
    private var personList = [Person]()
    private var nextStringParam: String?
    private var isInitialRequest = true
    private var personListAsSet =  Set<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        generatePersonList()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(refreshController)
        tableView.tableFooterView = UIView() // for empty cells
    }
    
    private func generatePersonList(next: String? = nil) {
        DataSource.fetch(next: next) {[weak self] (response, error) in
            guard let self = self else { return }
            if let validResponse = response, error != nil {
                self.checkDuplicatedData(array: validResponse.people)
                self.nextStringParam = validResponse.next
                self.refreshController.endRefreshing()
                self.isInitialRequest = false
                self.tableView.tableFooterView = UIView()
                self.tableView.reloadData()
            } else {
                self.tableView.tableFooterView = self.endOfListLabel
                self.refreshController.endRefreshing()
            }
        }
    }
    
    @objc func reloadData() {
        self.isInitialRequest = true
        self.generatePersonList()
    }
    
    private func checkIsEmpty() {
        self.emptyLabel.isHidden = !self.personList.isEmpty
    }
    
    private func checkDuplicatedData(array: [Person]) {
        if isInitialRequest {
            self.personList = [Person]()
            self.personListAsSet.removeAll()
        }
        var newValueList = [Person]()
        array.forEach { (item) in
            self.personListAsSet.insert(item.id)
        }
        
        for index in 0..<array.count {
            if self.personListAsSet.contains(array[index].id) {
                newValueList.append(array[index])
            }
        }
        
        self.personList += newValueList
        checkIsEmpty()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        personList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let person = personList[indexPath.row]
        cell.textLabel?.text = person.fullName + " (\(person.id))"
        cell.selectionStyle = .none // for no action
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = self.personList.count - 1
        if indexPath.row == lastItem {
            generatePersonList(next: self.nextStringParam)
        }
    }
    
}
